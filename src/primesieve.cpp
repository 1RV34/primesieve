#include <boost/range/adaptor/transformed.hpp>
#include <boost/algorithm/string/join.hpp>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include "app.hpp"

using namespace app;

int main(int argc, char* argv[])
{
    /*
    |--------------------------------------------------------------------------
    | Validate arguments
    |--------------------------------------------------------------------------
    |
    | The first argument is the maximum number to check for primes.
    | The second argument is the minimum number to check for primes. (Currently not used)
    | If the second argument is not provided, it defaults to 2.
    |
    */

    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " MAX" << std::endl;

        return 1;
    }

    int max;
    std::string arg2 = argv[1];

    try {
        std::size_t pos;
        max = std::stoi(arg2, &pos);

        if (pos < arg2.size()) {
            std::cerr << "Trailing characters after number: " << arg2 << std::endl;

            return 1;
        }
    } catch (std::invalid_argument const &exception) {
        std::cerr << "Invalid number: " << arg2 << std::endl;

        return 1;
    } catch (std::out_of_range const &exception) {
        std::cerr << "Number out of range: " << arg2 << std::endl;

        return 1;
    }

    int min;

    // if (argc > 2) {
    //     std::string arg1 = argv[2];

    //     try {
    //         std::size_t pos;
    //         min = std::stoi(arg1, &pos);

    //         if (pos < arg1.size()) {
    //             std::cerr << "Trailing characters after number: " << arg1 << std::endl;

    //             return 1;
    //         }
    //     } catch (std::invalid_argument const &exception) {
    //         std::cerr << "Invalid number: " << arg1 << std::endl;

    //         return 1;
    //     } catch (std::out_of_range const &exception) {
    //         std::cerr << "Number out of range: " << arg1 << std::endl;

    //         return 1;
    //     }

    //     if (min < 2) {
    //         std::cerr << "MIN must be greater than or equal to 2" << std::endl;

    //         return 1;
    //     }
    // } else {
        min = 2;
    // }

    if (max < min) {
        std::cerr << "MAX must be greater than or equal to MIN" << std::endl;

        return 1;
    }

    std::vector<bool> primes;

    if (1 + max - min > primes.max_size()) {
        std::cerr << "The difference between MIN and MAX must be less than or equal to " << primes.max_size() << std::endl;

        return 1;
    }

    /*
    |--------------------------------------------------------------------------
    | Create a vector of booleans
    |--------------------------------------------------------------------------
    |
    | The vector is initialized to true for all values.
    | The vector is used to mark numbers as prime or not prime.
    |
    */

    primes = std::vector<bool>(1 + max - min, true);

    /*
    |--------------------------------------------------------------------------
    | Sieve of Eratosthenes
    |--------------------------------------------------------------------------
    |
    | The sieve of Eratosthenes is used to find all prime numbers up to MAX.
    | The sieve of Eratosthenes is an algorithm for finding all prime numbers up to any given limit.
    | It does so by iteratively marking as composite (i.e., not prime) the multiples of each prime,
    | starting with the first prime number, 2.
    |
    */

    // Stuff goes wrong here when using a different MIN, have to think about it
    for (int i = min; i <= std::sqrt(max); i++) {
        if (primes[i - min]) {
            for (int j = i * i; j <= max; j += i) {
                primes[j - min] = false;
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Convert vector of booleans to vector of prime numbers
    |--------------------------------------------------------------------------
    |
    | The vector of booleans is converted to a vector of prime numbers.
    |
    */

    std::vector<int> primeNumbers;

    for (int i = min; i <= max; i++) {
        if (primes[i - min]) {
            primeNumbers.push_back(i);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Output prime numbers
    |--------------------------------------------------------------------------
    |
    | The prime numbers are output to the console.
    |
    */

    std::cout << boost::algorithm::join(
        primeNumbers | boost::adaptors::transformed([](int i) { return std::to_string(i); }),
        "\t"
    ) << std::endl;

    /*
    |--------------------------------------------------------------------------
    | Return
    |--------------------------------------------------------------------------
    |
    | Return 0 to indicate success :)
    |
    */

    return 0;
}
