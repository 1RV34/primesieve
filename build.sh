#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

mkdir -p build

g++ -lstdc++ -I /usr/include/boost -o build/primesieve src/*.cpp
